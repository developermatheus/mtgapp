import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";

@Injectable()
export class CardProvider {

  hostApi: string = 'https://api.magicthegathering.io/v1';

  constructor(public http: HttpClient) {
    console.log('Hello PokemonProvider Provider');
  }

  // getAllCards(page: number) {
  //   return new Promise(resolve => {
  //     let url = this.hostApi + '/cards?page=' + page;
  //     this.http.get(url)
  //     .subscribe(data => {
  //       resolve(data['cards']);
  //     });
  //   });
  // }

  // getCardDetail(idCard: string) {
  //   return new Promise(resolve => {
  //     let url = this.hostApi + '/cards/' + idCard;
  //     this.http.get(url)
  //     .subscribe(data => {
  //       resolve(data['card']);
  //     });
  //   });
  // }
  
  getCardByName(cardName: string, page:number) {
    console.log(cardName);
    return new Promise(resolve => {
      let url = this.hostApi + '/cards?name=' + cardName + '&page=' + page;
      console.log(url);
      this.http.get(url)
      .subscribe(data => {
        resolve(data['cards'])
      });
    });
  }

}
