import { Injectable } from '@angular/core';
import { 
  Alert,  
  AlertController,
  AlertOptions,
  ActionSheet, 
  ActionSheetController,
  Loading, 
  LoadingController,
  LoadingOptions,
  MenuController,
  Modal,
  ModalController,
  Platform,
  Toast,
  ToastController,
  ToastOptions,
  TextInput
} from 'ionic-angular';

@Injectable()
export class UtilsProvider {
  loading: Loading;

  constructor(  
    private actionCtrl: ActionSheetController,
    private alertCtrl : AlertController, 
    private loadCtrl  : LoadingController,
    private menuCtrl  : MenuController,
    private modalCtrl : ModalController,
    private toastCtrl : ToastController,
    private platform  : Platform
  ) {
    console.log('Hello UtilsProvider Provider');
  }

  //implementar posteriormente
  showAction(btns) {
    // let options: ActionSheetOptions = {};
    const action: ActionSheet = this.actionCtrl.create({
        buttons: btns
      });

    action.present();
  }

  changeStateMenu(menu:string, state: boolean) {
    this.menuCtrl.enable(state, menu);
  }

  menuClose() {
    this.menuCtrl.close();
  }
  
  showSimpleAlert(msg:string, title?: string, btns?: any[], inpt?: any[]) {
    if (!btns) {
      btns = [
        {
          text: 'Ok',
          role: 'cancel'
        }
      ]
    };
    
    let options: AlertOptions = {
      message: msg, 
      title: title,
      buttons: btns,
      inputs: inpt
    }
    let alert: Alert = this.alertCtrl.create(options);
    alert.present();
  }

  showLoading(msg: string = 'Por favor, aguarde...', title?: string ) {
    let options: LoadingOptions = {
      content: msg, 
    }
    
    this.loading = this.loadCtrl.create(options);
    this.loading.present();
  }

  stopLoading() {
    this.loading.dismiss();
  }

  showToast(msg: string, duration: number = 3000, position: string = 'bottom') {
    let options: ToastOptions = {
      message: msg, 
      duration: duration, 
      position: position,
    }

    let toast: Toast = this.toastCtrl.create(options);
    toast.present();
  }

  checkPlatform(type: string): boolean {
    return this.platform.is(type);
  }

  createModal(page: string, params?: object): Modal {
    let modal: Modal = this.modalCtrl.create(page, params);
    return modal;
  }

  setFocusOnInput(input: TextInput) {
    setTimeout(() => {
      input.setFocus();
    }, 300);
  }
}

