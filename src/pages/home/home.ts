import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { CardProvider } from '../../providers/card/card';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  @ViewChild(Content) content: Content;
  cards: any;
  page: number;
  searchControl: FormControl;
  searching: boolean = false;
  cardName: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cardPro: CardProvider) {
      this.searchControl = new FormControl();
      this.page = 1;
  }

  ionViewDidLoad() {
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.getCardByName(this.cardName);
    });
  }

  onSearchInput() {
    this.page = 1;
    this.searching = true;
  }

  getCardByName(name: string) {
    this.cardPro.getCardByName(name, this.page)
      .then(data => {
        this.cards = data;
        console.log("card isolado: ", this.cards);
      });
  }

  goToCardDetail(card: Object) {
    this.navCtrl.push("CardDetailPage", { card: card });
  }

  doInfinite(infiniteScroll) {
    this.page++;
    console.log(this.page);
    setTimeout(() => {
      this.cardPro.getCardByName(this.cardName, this.page)
        .then(data => {
          const newCards: any = data;
          this.cards = this.cards.concat(newCards);
          console.log("teste", this.cards);
        });
      infiniteScroll.complete();
    }, 500);
  }

  goUp() {
    this.content.scrollToTop();
  }
  
}
