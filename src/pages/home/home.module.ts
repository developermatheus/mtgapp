import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { AutoHideDirective } from '../../directives/auto-hide/auto-hide';

@NgModule({
  declarations: [
    HomePage,
    AutoHideDirective
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}
