import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-card-detail',
  templateUrl: 'card-detail.html',
})
export class CardDetailPage {
  card: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
      this.card = this.navParams.get("card");
      console.log("detalhe do card: ", this.card);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardDetailPage');
  }

}
